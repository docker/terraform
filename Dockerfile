FROM registry.gitlab.com/gitlab-org/terraform-images/stable:latest
LABEL maintainer="Guillaume Charifi <guillaume.charifi@sfr.fr>"

COPY src/ /

RUN \
        curl -L "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl" -o /bin/kubectl; \
        chmod 755 /bin/kubectl \
                /bin/kube-service-get-port;
